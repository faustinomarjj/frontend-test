import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./User/Login";
import Register from "../components/User/Register";
import HomePage from "./HomePage/HomePage";
import "../App.css";

const Index = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const checkLoggedInStatus = () => {
      const isLoggedIn = localStorage.getItem("isLoggedIn") === "true";
      setIsLoggedIn(isLoggedIn);
    };

    checkLoggedInStatus();
  }, []);

  return (
    <div className="App">
      <Router>
        <Switch>
          <Route
            exact
            path="/"
            component={() => <Login setIsLoggedIn={setIsLoggedIn} />}
          />
          <Route path="/register" component={Register} />
          <Route path="/home" component={HomePage} />
        </Switch>
      </Router>
    </div>
  );
};

export default Index;
