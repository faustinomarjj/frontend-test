import React from "react";
import Sidebar from "../Sidebar/Sidebar";
import Header from "../Header/Header";
import ProductStatus from "../Product/ProductStatus/ProductStatus";
import ProductList from "../Product/ProductList/ProductList";
import ProductAddButton from "../Product/ProductAdd/ProductAddButton";
import ProductForm from "../Product/ProductAdd/ProductAddForm";
import DashboardRouter from "../Router/DashboardRouter";
import ProductRouter from "../Router/ProductRouter";
import AddProductRouter from "../Router/AddProductRouter";

function HomePage() {
  return (
    <>
      <Sidebar />
      {/* <Header /> */}
      {/* <ProductStatus /> */}
      {/* <ProductAddButton />
      {/* <ProductList /> */}
      {/* <ProductForm /> */}
      {/* </Sidebar>  */}
      {/* <DashboardRouter /> */}
      {/* <ProductRouter /> */}
      {/* <AddProductRouter /> */}
    </>
  );
}

export default HomePage;
