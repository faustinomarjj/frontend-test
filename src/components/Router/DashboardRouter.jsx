import React, { useState, useEffect } from "react";
import Sidebar from "../Sidebar/Sidebar";
import Header from "../Header/Header";
import ProductStatus from "../Product/ProductStatus/ProductStatus";
import ProductList from "../Product/ProductList/ProductList";
import ProductAddButton from "../Product/ProductAdd/ProductAddButton";
import AddProductRouter from "../Router/AddProductRouter";

function Dashboard() {
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    fetchProducts();
  }, []);

  const fetchProducts = async () => {
    try {
      setIsLoading(true);
      const response = await fetch("http://localhost:3000/getProducts", {
        method: "POST",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });

      console.log(response);
      if (!response.ok) {
        throw new Error("Failed to fetch products");
      }
      const data = await response.json();
      setProducts(data);
      setIsLoading(false);
    } catch (error) {
      console.error("Error:", error);
      setIsLoading(false);
    }
  };

  return (
    <>
      <Header />
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        <ProductStatus products={products} />
      )}
      <ProductList />
    </>
  );
}

export default Dashboard;
